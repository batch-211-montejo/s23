
let trainer = {
	name: "Ash Ketchun",
	age: 23,
	pokemon: ["pikachu" , "bulbasaur", "snorlax"],
	friends: {
		hoen: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function (){
		console.log("Pikachu! I choose you!")
	}
}
console.log(trainer)
console.log(trainer.friends)

console.log("Result of dot notation");
console.log(trainer.name)
console.log("Result of square bracket notation");
console.log(trainer["pokemon"])

console.log("Result of talk method")
trainer.talk()

function Pokemon(name,level, health, attack){
			//properties
			this.name = name;
			this.level = level;
			this.health = level * 2;
			this.attack = level*1.5;
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);

				target.health -= this.attack

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health<=0){
					target.faint()
				}

			}
			this.faint = function(){
				console.log(this.name + " fainted.")
			}

		}
	let pikachu = new Pokemon ("Pikachu", 12, 24, 12);
		console.log(pikachu);

	let geodude = new Pokemon ("Geodude", 8, 16, 8);
		console.log(geodude);

	let meowtwo = new Pokemon ("Meowtwo", 100, 200, 100);
	console.log(meowtwo);