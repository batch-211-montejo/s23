// console.log("Hello World")

// Object
/*
	-an onject is a data type that os used to represemt real world objects
	-it is collection of related data and/or functionalities
	-info stored in object are represented a "key:value" pair

	-"key" = a property of an object
	-"value" - actual ata to be stored

	-differemet daya type may be stored in an object's property creating complex data structures
*/

	// There are 2 ways in creating objects in JS
		// 1. Object Literal notation 
			// (let object = {}) **curly braces/object literals

		// 2. Object Constructor Notation
			// Object Instantaion (let object = new Object())



///////////////////////

// Object Literatopm Notation
	// creating objects using initializer/literal notation
	// camelCase

	/*
		Syntax:

			let objectName = {
				keyA : valueA,
				keyB : valueB
			}
	*/
	
	let cellphone = {
		name: "nokia 3210",
		manufactureDate: 1999
	}
	console.log("Resukt from creating objects using literal notation")
	console.log(cellphone)


	// miniactivity

	let cellphone2 = {
		name: "iPhone 4s",
		manufactureDate: 2011
	}
	console.log(cellphone2)

	// another example
	let ninja = {
		name: "Naruto Uzumaki",
		village: "Konoha",
		childrer: ["Baruto", "Himawari"]
	}
	console.log(ninja)


///////////////////////////////////////////////
// Object Constructor Notation
	// creating objects using a constructor function
		// creates a resuable "function" tp create seceral objects that have the same data structure

		/*
			Syntax:
				function objectName(keyA,keyB) {
					this.keyA = keyA;
					this.keyB = keyB;
				}
		*/


		// "this" key work refers to the properties within the object
			// it allows the assignement of new object's properties 
			// by associating thm with calues received from the constructor functions's parameter (blueprint)
		function Laptop(name, manufactureDate){
			this.name = name;
			this.manufactureDate = manufactureDate;
		}
		// create an instance object using the laptop constructor
		let laptop = new Laptop("Lenovo", 2008);
		console.log("Result from creating objects using objects constructor");
		console.log(laptop)


		// the new operator creates an instance of an object(new object)
		let myLaptop = new Laptop ("Macbook Air", 2020);
		console.log("Result from creating objects using objects constructor");
		console.log(myLaptop)


		//miniactivity
		let laptop1 = new Laptop ("Huawei D15", 2020)
		let laptop2 = new Laptop ("Macbook Pro", 2020)
		let laptop3 = new Laptop ("Huawei D14", 2021)
		console.log(laptop1)
		console.log(laptop2)
		console.log(laptop3)

		// another example
		let oldLaptop = Laptop ("Portal R2E CCMC", 1980);
		console.log("Result from creating objects using objects constructor");
		console.log(oldLaptop) //undefined
		// "new" serves as return statement

	
//////////////////////////////////////
// Create empty objects
	let computer = {};
	// another way of creating empty objects
	let myComputer = new Object()

	console.log(computer) //{}
	console.log(myComputer) //{}


// Accessing Object Properties
	// using dot notation -- highly recommended to use
		console.log("Result from dot notation: " + myLaptop.name); //Macbook Air

	// using square bracket notation -- looks like array
		console.log("Result from square bracket notation: " + myLaptop["name"]); //Macbook Air

// Accessing array of ocjtecs
// Accessing obkecy properties using the square bracket notation adn array indexes can cause confusion

	let arrayObj = [laptop,myLaptop];
	// we may be confused for accessing array indexes
	console.log(arrayObj[0]["name"]) //Lenovo

	// this tells is that array[0] is an object by using the dot notation
	console.log(arrayObj[0].name); //Lenovo



///////////////////////////////////////////////////////////

// Initializing/Adding/Deleting/Reassignin Object Properties

	// Create an object using object literals
	let car = {};
	console.log("Current valur of car objects: ");
	console.log(car)


	// Initializing/adding object properties
	car.name = "Honda Civic";
	console.log("Result from adding properties using dot notation");
	console.log(car) // {name: "Honda Civic"}


	// Initializing/adding object properties using bracket notation (not recommended)
	// can use space
	console.log("Result from adding properties using bracket notation");
	car["manufacture date"] = 2019;
	console.log(car) // {name: "Honda Civic", manufacture date: 2019}


	// Deleting Object Properties
	delete car["manufacture date"];
	console.log("Result from deleting properties");
	console.log(car) //{name: "Honda Civic"}


	car["manufactureDate"] = 2019;
	console.log(car)


	// Reassigning Object Property Values
	car.name = "Toyota Vios";
	console.log("Result from reassigning property values:")
	console.log(car) //{name: "Toyota Vios", manufacture date: 2019}

	delete car.manufactureDate // deleter a peroperty
	console.log(car)



// Object Methods
	// a method is a function which is a property of an object


	let person = {
		name: "John",
		talk: function () {
			console.log("Hello my name is " + this.name)
		}
	}
	console.log(person)
	console.log("Result from object methods: ");
	person.talk();


	// 
	person.walk = function(steps) {
		console.log(this.name + " walked " + steps + " steps forward");
	}
	console.log(person);
	person.walk(50); // person walked 50 steps forward

	// methods are useful for creating reusable functions that perform tasks relared to objects

	let friend = {
		firstName: "Joe",
		lastName: "Smith",
		address: {
			city: "Austin",
			country: "Texas"
		},
		emails: ["joe@mail.com", "joesmith@maul.xyz"], 
		introduce: function () {
			console.log("Hello my name is " + this.firstName + " " + this.lastName + ". " + "I live in " + this.address.city + ", " + this.address.country)
		}
	}
	friend.introduce();


////////////////////////////////////

// Real world application of objects
/*
	Scenario:
		1. we would like to create a game that would have several pokemon interact with each other
		2. Every Pokemon would have the same set of stats, properties, and function

		Stats:
		name:
		level:
		health: level * 2
		attach: level
*/

	// create an objectc constructore to lessn the process in creating the pokemon

	function Pokemon(name,level){

			//properties
			this.name = name;
			this.level = level;
			this.health = level * 2;
			this.attack = level;
			this.tackle = function(target){
				console.log(this.name + " tackled " + target.name);

				//Mini Activity
				//if the target health is less than or equal to 0 we will invoke the faint(method), otherwise printout the pokemon's new health
				//example if health is not less than 0
					//rattata's health is now reduced to 5

				//reduces the target object's health property by subtracting and reassigning it's value based on the pokemon's attack

				// target.health = target.health - this.attack
				target.health -= this.attack

				console.log(target.name + " health is now reduced to " + target.health);

				if(target.health<=0){
					target.faint()
				}

			}
			this.faint = function(){
				console.log(this.name + " fainted.")
			}

		}

		let pikachu = new Pokemon ("Pikachu", 88);
		console.log(pikachu);

		let rattata = new Pokemon("Ratata", 10);
		console.log(rattata); 
